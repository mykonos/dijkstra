import de.kotlincook.dijkstra.graph.Dijkstra
import de.kotlincook.dijkstra.graph.Graph
import de.kotlincook.dijkstra.graph.Vertex

fun main() {
    val a = Vertex("A")
    val b = Vertex("B")
    val c = Vertex("C")
    val d = Vertex("D")
    val e = Vertex("E")
    val f = Vertex("F")
    val g = Vertex("G")
    val h = Vertex("H")
    val i = Vertex("I")
    val j = Vertex("J")

    val graph = Graph()
    graph.addEdge(a, b, 16.0)
    graph.addEdge(a, c, 17.0)
    graph.addEdge(b, e, 26.0)
    graph.addEdge(c, d, 15.0)
    graph.addEdge(d, a, 7.0)
    graph.addEdge(c, f, 8.0)
    graph.addEdge(d, h, 32.0)
    graph.addEdge(d, f, 34.0)
    graph.addEdge(d, a, 14.0)
    graph.addEdge(e, h, 12.0)
    graph.addEdge(f, g, 9.0)
    graph.addEdge(f, i, 14.0)
    graph.addEdge(g, i, 4.0)
    graph.addEdge(g, h, 7.0)
    graph.addEdge(g, j, 16.0)
    graph.addEdge(h, j, 10.0)
    graph.addEdge(i, j, 9.0)

    val dijkstra = Dijkstra(graph)
    println(dijkstra.run(a, j))
}