package de.kotlincook.dijkstra.graph

import kotlin.math.min

class Dijkstra(val graph: Graph) {

    val distToStart = mutableMapOf<Vertex, Double>()
    val visited = mutableMapOf<Vertex, Boolean>()

    fun run(start: Vertex, dest: Vertex): Double {
        init(start)

        val unsettled = mutableSetOf<Vertex>()
        unsettled += start
        while (unsettled.isNotEmpty()) {
            val lowestDistVertex = unsettled.minBy { v -> distToStart[v]!! }
            unsettled.remove(lowestDistVertex)
            visited[lowestDistVertex] = true
            graph.neighbours(lowestDistVertex)
                ?.forEach {
                    distToStart[it] = min(
                        distToStart[it]!!,
                        distToStart[lowestDistVertex]!! + graph.weight(lowestDistVertex, it)!!
                    )
                    if (!visited[it]!!) unsettled += it
                }
        }
        return distToStart[dest]!!
    }

    private fun init(start: Vertex) {
        graph.vertices().forEach {
            visited[it] = false
            distToStart[it] = Double.MAX_VALUE
        }
        distToStart[start] = 0.0
    }
}


