package de.kotlincook.dijkstra.graph

data class Vertex(val name: String)
