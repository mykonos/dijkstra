package de.kotlincook.dijkstra.graph

class Graph {

    private var vertices: MutableMap<Vertex, MutableSet<Vertex>> = mutableMapOf()
    private var edgeWeights: MutableMap<Pair<Vertex, Vertex>, Double> = mutableMapOf()

    fun addEdge(from: Vertex, to: Vertex, weight: Double) {
        if (vertices[from] == null) {
            vertices[from] = mutableSetOf()
        }
        if (vertices[to] == null) {
            vertices[to] = mutableSetOf()
        }
        vertices[from]!! += to
        edgeWeights[Pair(from, to)] = weight
    }

    fun neighbours(vertex: Vertex) = vertices[vertex]?.toSet()

    fun weight(from: Vertex, to: Vertex): Double? = edgeWeights[Pair(from, to)]

    fun vertices(): Set<Vertex> = vertices.keys

}